import { z, defineCollection } from 'astro:content';

const skillCollection = defineCollection({
  type: "content",
  schema: z.object({
    title: z.string(),
    level: z.number(),
    imgsrc: z.string(),
    imgalt: z.string(),
    description: z.string(),
    tags: z.array(z.string())
  })
});

const projectCollection = defineCollection({
  type: "content",
  schema: z.object({
    title: z.string(),
    description: z.string(),
    imgsrc: z.string(),
    imgalt: z.string(),
    by: z.string(),
    clients: z.array(z.string()),
    tags: z.array(z.string())
  })
});

const backgroundCollection = defineCollection({
  type: "content",
  schema: z.object({
    type: z.enum(["education", "pro"]),
    title: z.string(),
    period: z.string(),
    diploma: z.optional(z.string()),
    job: z.optional(z.string()),
    location: z.string(),
    link: z.string(),
    imgsrc: z.string(),
    imgalt: z.string(),
  })
});

const certCollection = defineCollection({
  type: "data",
  schema: z.object({
    type: z.enum(["cert"]),
    title: z.string(),
    link: z.optional(z.string()),
    date: z.string(),
    imgsrc: z.string(),
    imgalt: z.string(),
    desc: z.string(),
  })
});

export const collections = {
  skills: skillCollection,
  projects: projectCollection,
  background: backgroundCollection,
  cert: certCollection,
};