const path = require('path');

module.exports = {
  plugins: {
    tailwindcss: {
      config: path.join(__direname, 'tailwind.config.js'),
    },
    autoprefixer: {}
  }
}