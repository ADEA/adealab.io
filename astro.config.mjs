import { defineConfig } from 'astro/config';
import sitemap from "@astrojs/sitemap";
import compress from "astro-compress";
import { astroImageTools } from "astro-imagetools";
import robotsTxt from "astro-robots-txt";
import mdx from "@astrojs/mdx";

import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  site: "https://adea.gitlab.io",
  integrations: [astroImageTools, sitemap(), compress({
    path: 'public'
  }), robotsTxt(), mdx(), tailwind()],
  outDir: 'public',
  publicDir: 'static'
});