---
title: Scripting
level: 4
imgsrc: https://geekflare.com/wp-content/uploads/2021/04/scripting-languages-1200x385.jpg
imgalt: Dessin représentant le scripting
description: L'automatisation par assemblage de petits programmes.
tags:
  - programmation
  - fullstack
---

<img src={frontmatter.imgsrc} alt={frontmatter.imgalt} class="max-w-none w-full max-h-28" style="object-fit:cover"/>

# Scripting

## Présentation

Le **scripting** fait référence à l'écriture de scripts, qui sont des _programmes informatiques_ écrits dans un langage de script. Les scripts sont généralement utilisés pour _automatiser_ des tâches répétitives ou pour effectuer des opérations complexes sur des données.

Pour **automatiser les processus**, nous pouvons _programmer des applications_ qui font ces actions. Comme ce sont des actions assez simples mais multiples, il nous faut des dizaines ou centaines de programmes _très courts et simples à lire et écrire_. D'où l'importance du scripting aujourd'hui.

Les scripts peuvent être utilisés pour une variété de tâches, telles que la **gestion de fichiers**, la **manipulation de données**, la **configuration de logiciels ou de systèmes**, et la création de sites Web dynamiques. Les scripts sont souvent utilisés dans les environnements de lignes de commande, mais ils peuvent également être intégrés à des applications graphiques ou à des sites Web.

Je recommande le très court article en anglais ["What Is Scripting and What Is It Used For?"](https://www.bestcolleges.com/bootcamps/guides/what-is-scripting/), qui explique de façon succincte l'importance du scripting aujourd'hui.

<hr />

# Mon expérience

Il est important, dans l'informatique, de pouvoir s'adapter à toute situation et d'avoir des compétences dans chaque domaine du cycle de vie d'un logiciel. De plus, le cœur même de **DevOps** est l'automatisation, qui est associée au Scripting. Il est donc important de pouvoir écrire des scripts pour répondre à tout type de tâche. À mon poste, c'est une compétence cruciale, car j'écris des scripts presque tous les jours. J'automatise les tâches, les programmes, les tests, les déploiements, et les messages que j'envoie lorsque j'ai fini mon travail.

Un premier projet où j'ai beaucoup utilisé le Scripting est le projet **Liveboard**, une solution de supervision de données qu'Aberia a développée et vendue à plusieurs clients. Tous les clients n'ont pas les mêmes serveurs et bases de données, certains utilisent des schémas différents. Il est donc impossible de porter la solution existante telle qu'elle.

J'ai donc utilisé des connecteurs qui appellent, pour chaque donnée demandée, un script différent. Ce script va lui-même en appeler d'autres pour simplifier la demande de données. Voici une liste non exhaustive des langages de Scripting que j'ai pu utiliser dans ce projet.

- **Bash** : exécution de multiples scripts en ligne de commande
- **Sed** : édition simple de chaînes de caractères
- **Awk** : édition complexe de chaînes de caractères
- **jq** : formatage d'objet _JSON_
- **Python** : traitement de données
- **PHP** : formatage de documents _XML_
- **JavaScript** : script complexe généralisé
- **Makefile** : chaine de build

[Lien vers le projet **Liveboard**](/projects/liveboard)

Un autre projet dans lequel j'ai multiplié l'utilisation de scripts est le projet **UnicampRewritten**, la réécriture d'un service _Windows Server_ qui écoute des requêtes **HTTP(S)** pour écrire en base de données.

Ce service est simple, il ne fait qu'écouter une seule entrée, et insère les données passées dans le serveur. Si le projet semble simple, c'est parce que nous n'avons pas encore énoncé les contraintes : le programme doit être natif à _Windows Server_, il faut donc utiliser un langage compilé ou interprété nativement par le système. Il doit être rétro compatible, donc impossible d'utiliser les dernières innovations. Enfin, tout doit être extrêmement simple et intuitif à installer et utiliser.

Après avoir passé un moment à faire du _reverse engineering_ sur la solution existante, j'ai redéveloppé le service en **C#** avec **.NET Framework 4.2**.

Pour pouvoir modifier la configuration **XML**, j'ai utilisé un script **[VBA](https://fr.wikipedia.org/wiki/Visual_Basic_for_Applications)** (à ne pas confondre avec **[VB.NET](https://fr.wikipedia.org/wiki/Visual_Basic_.NET)**). Pour pouvoir enregistrer ou supprimer le service Windows ajouté par le programme, j'ai utilisé un script **Batch**. Ensuite, j'ai créé l'installateur du programme qui se charge d'installer les fichiers et lancer les scripts grâce à [**NSIS** (Nullsoft Scriptable Install System)](https://nsis.sourceforge.io/) qui est ensuite compilé. Les textes d'informations devant être au format _RTF_ (Wordpad) au lieu d'un format _Markdown_ plus habituel, j'ai développé un script de conversion avec **Perl**. Enfin, ma chaîne de build a été automatisé avec **PowerShell**.

[Lien vers le projet **UnicampRewritten**](/projects/unicamp-rewritten)

<hr />

## Autocritique

Je me considère assez bon dans l'art du script, mais je dois m'améliorer sur l'utilisation de plusieurs langages tels que **Ruby**, **Raku**, **Groovy** ou **Lua** pour ne citer que les plus connus. Il me reste beaucoup à apprendre, mais je pense avoir compris la méthode. J'ai l'habitude de créer des scripts bien plus que de _vrais_ programmes.

Depuis deux ans, je fais des scripts d'automatisation au sein de mon entreprise. Je me rappelle avoir commencé le Scripting pour éviter de répéter plusieurs commandes lors de mes tests. Ensuite, j'ai commencé à formatter du texte avec **Awk**, et je n'ai plus arrêté de faire du script depuis. Je suis passé de débutant à autonome.

Depuis ma découverte de cet art, de nouveaux outils sortent régulièrement, d'autres tombent dans l'oubli. Il reste important de se mettre à jour pour pouvoir maintenir ces petites briques qui portent nos murs. Je n'ai pas vraiment de conseil à donner, à part de bien choisir ses ressources et de persévérer.

Cette compétence est importante dans mon profil professionnel. Gérer l'automatisation des processus avec des scripts est primordial si ce n'est le pilier central de mon travail. Je souhaite donc me perfectionner pour continuer à écrire ces courts programmes qui s'avèrent tellement utiles.

Afin de m'améliorer, j'aimerais suivre une formation sur certaines technologies comme **PowerShell**. Je pense aussi acheter quelques livres des éditions [_O'Reilly_](https://www.oreilly.com/products/books-videos.html) qui sont réputées dans la programmation pour leur qualité.
