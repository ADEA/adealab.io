import { defineConfig } from "astro-imagetools/config";

export default defineConfig({
  loading: "lazy",
  decoding: "async",
  breakpoints: [200, 400, 800, 1600],
  format: ["png"],
  includeSourceFormat: true,
  quality: 80,
  placeholder: 'tracedSVG',
  formatOptions: {
    tracedSVG: {
      function: 'posterize'
    }
  }
});

