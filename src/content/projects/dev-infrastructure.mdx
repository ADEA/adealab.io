---
title: Infra Dev
by: Aberia
clients: ["Aberia"]
imgsrc: https://cdn.openpr.com/T/8/T827717221_g.jpg
imgalt: Image représentant l'infrastructure informatique
description: Une infrastructure faite par les développeurs, pour les développeurs, comprenant tous nos besoins sur l'intégralité du cycle DevOps.
tags:
  - infra
  - linux
  - gitlab
---

<img src={frontmatter.imgsrc} alt={frontmatter.imgalt} class="max-w-none w-full max-h-28" style="object-fit:cover"/>

# Infra Dev

## Contexte

Le développement est une tâche compliquée qui se complexifie de jour en jour. Lointaine est l'époque où l'on pouvait _simplement_ écrire notre code, le compiler localement et l'envoyer au client (si cette époque a existé dans le domaine de l'entreprise).

En effet, il y a de nombreuses tâches liées au développement qu'il ne faut pas oublier :

- Sauvegarder son code ;
- Dans le cas d'une équipe, fusionner le code de plusieurs membres ;
- Tester le code après chaque nouvelle fonctionnalité ;
- Communiquer sur l'avancement du projet ;
- Livrer les nouvelles versions en cas de mise à jour ;
- Et bien d'autres.

Évidemment, les développeurs n'aiment pas toujours s'occuper de toutes ces tâches, surtout quand elles sont compliquées à mettre en place ou prennent un certain temps. C'est pourquoi de nombreux outils ont été créés afin de simplifier le processus, permettant de mettre en place une **infrastructure de développement**. C'est un ensemble d'outils, de technologies et de ressources qui permettent aux développeurs de créer, tester et déployer des logiciels. Il est fréquent dans ce type d'infrastructure d'inclure des serveurs de développement, des [systèmes de gestion de version de code](https://fr.wikipedia.org/wiki/Logiciel_de_gestion_de_versions), des outils d'automatisation, et plus encore.

Créer une infrastructure avec tous ces outils est une tâche fastidieuse, mais cette mise en place pourra ensuite simplifier le travail des développeurs et permettre aux autres membres de l'équipe (superviseurs, managers, designers, etc.) d'avoir une idée plus précise du travail accompli et restant à effectuer dans le projet. C'est donc un très fort avantage pour l'équipe, qui se traduit par un gain de temps, et donc d'argent pour l'entreprise.

Je pourrais décrire tout le cycle **DevOps** pour avancer tous les avantages de cette méthodologie, mais cela serait très long et ce n'est vraiment pas le sujet de cet article. Je veux surtout parler de la mise en place de cette infrastructure chez Aberia, projet que j'ai mené pour bénéficier de tous les avantages cités ci-dessus.

<hr />

## Mise en œuvre

Avant d'installer des outils, il faut savoir lesquels installer. Si cela semble une évidence, il reste toutefois important de le rappeler car il faut que l'infrastructure convienne à nos besoins à court et moyen terme, et qu'elle soit assez modulable pour nous faciliter une possible transition sur le long terme.
Étant le seul développeur dans mon équipe, cette infrastructure est surtout _pour moi_. Je dois garder à l'esprit qu'elle survivra après mon possible départ et qu'elle ne m'appartient pas, mais ne pas non plus oublier qu'elle doit faciliter _mon_ travail.

J'ai choisi d'installer la plateforme [GitLab](https://about.gitlab.com), une plateforme libre et _freemium_ (c'est-à-dire gratuite avec des fonctionnalités bloquées pour utilisateurs payants) de gestion du cycle DevOps. La plateforme permet autant la **gestion de projet** que la **gestion du code** et la **gestion des déploiements**. Si j'ai parlé d'installer la plateforme, c'est parce que je n'utilise pas la version en ligne mais une version directement maintenue en interne sur nos serveurs. Nous sommes donc maîtres de la plateforme et de nos données, une thématique assez importante de nos jours.

Une fois GitLab installé, je l'ai configuré : suppression du _monitoring_, réduction de l'utilisation des ressources, liaison avec des adresses mail officielles d'Aberia, etc. Si cette installation permet déjà de simplifier considérablement le _workflow_, il reste quelques ajouts à faire.

Je pense ici surtout à l'automatisation. Certaines tâches sont simples et extrêmement répétitives, comme l'exécution de tests préprogrammés ou la génération d'une documentation. Elles sont si simples qu'on peut en faire un script que l'on exécute périodiquement. Là encore, il est possible de faire mieux que de demander à un ingénieur de lancer une commande toutes les semaines. Heureusement, en installant des **GitLab Runners**, des programmes de lancement de tâches automatisées, nous pouvons faire en sorte que le code soit automatiquement vérifié, testé, compilé, et même déployé à chaque modification du projet. Pour cela, j'ai choisi d'installer un serveur [Kubernetes](https://kubernetes.io) lié à l'instance GitLab. Ce serveur permet de supporter l'architecture de conteneurs (voir l'article sur ma compétence Docker) et de simplifier sa gestion.

Enfin, il faut simplifier l'utilisation de cette infrastructure. C'est assez simple quand on a l'habitude de l'utiliser et qu'on l'a mise en place, mais un chef de projet arrivant sur un outil inconnu va avoir du mal à comprendre réellement l'avancée du projet. Pour cela, j'ai rendu les serveurs accessibles en https (avec un certificat _self-signed_) sur des sous-domaines de notre intranet ; j'ai ajouté des connexions vers Microsoft Teams et Outlook afin de tenir les gens informés de grands changements et de la sortie d'une nouvelle version ; j'ai documenté les procédures d'utilisation et de maintenance de l'infrastructure avant de proposer une formation aux futurs utilisateurs.

<hr />

## Résultats

Pour l'entreprise, c'est un outil important qui permet de garder une trace propre des projets de développements logiciels, passés, présents et futurs. Pour moi-même, ce projet a été l'occasion de m'améliorer en gestion de systèmes et de mieux comprendre tout le cycle de vie d'une application. 

L'infrastructure est encore maintenue et améliorée régulièrement. Dans les mois qui suivent, cela devrait ralentir pour que l'on puisse poser une infrastructure solide. Si les besoins en ressources augmentent, il sera possible de migrer l'installation de GitLab sur un plus gros serveur, possiblement dans un cluster Kubernetes avec un nœud dans chaque succursale d'Aberia.

Enfin, si j'essaie de regarder ce projet avec du recul, je vois plusieurs manières dont il aurait pu être amélioré.
Je sais qu'il existe des outils comme **FluxCD** qui permettent d'automatiser les déploiements Kubernetes plus facilement ; je n'ai pas de certificats de sécurité valides pour le site, et je dois utiliser des _self-signed_ qui sont donc moins sécurisés ; enfin, je pourrais connecter les serveurs à nos outils de monitoring et de gestion de machines virtuelles, ce qui simplifierait davantage la maintenance de cette infrastructure.
Le projet étant encore en évolution, la plupart de ces changements peuvent encore être planifiés.

<hr />

## Acteurs principaux

- Adam Ambrosino : DevOps
- Stéphane Hernandez : Chef de projet
- Romain Hamerman : Administrateur Réseaux

<hr />

## Compétences

- [Linux](/skills/linux)
- DevOps
- Infrastructure
