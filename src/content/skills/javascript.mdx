---
title: (Vanilla) JavaScript
level: 5
imgsrc: https://licencias.info/wp-content/uploads/2019/07/javascript.jpg
imgalt: Image présentant le logo de JavaScript et un exemple de code
description: Le langage de scripting pour le web.
tags:
  - webdev
  - programmation
  - fullstack
---

<img src={frontmatter.imgsrc} alt={frontmatter.imgalt} class="max-w-none w-full max-h-28" style="object-fit:cover"/>

# JavaScript

## Présentation

JavaScript, aussi connu sous le nom d'ECMAscript, est un langage de scripting pour le web.
Ce langage créé en 10 jours par Brendan Eich en 1996 est aujourd'hui l’un des langages de programmation les plus utilisés.
C'est aujourd'hui le langage **de facto** du web, utilisé par tous les navigateurs majeurs.

Lors d'une commande d'application, les clients demandent une portabilité :

- l'application doit tourner sur différents systèmes d'exploitation,
- l'application doit être adaptée à différents modes d'affichage,
- l'application doit être simple d'accès,
- etc.

Une solution simple à ces contraintes est de développer des applications web, c'est-à-dire qui tournent dans un navigateur !
Les navigateurs deviennent une norme, et l'avènement de technologies comme [V8](https://v8.dev) ou [WebAssembly](https://webassembly.org) permettent d'en augmenter la puissance.
C'est pourquoi de nombreuses entreprises font ce choix de développer en priorité des solutions web avant de proposer des solutions dites natives, et pourquoi Javascript continue de grandir.

Jeff Atwood, co-fondateur des forums Stack Exchange et Stack Overflow, et figure emblématique du web, disait la chose suivante :
"Toute application qui peut être écrite en Javascript sera écrite en Javascript".

La popularité du langage JavaScript est une force. En effet, son écosystème riche donne fréquemment naissance à de nouvelles librairies ([Axios](https://axios-http.com), [Lodash](https://lodash.com), [Immutable.js](https://immutable-js.com), [chalk](https://github.com/chalk/chalk), etc.) et de nouveaux frameworks ([React](https://reactjs.org), [Angular](https://angular.io), [Vue](https://uejs.org), [SolidJS](https://www.solidjs.com), [Astro](https://astro.build), etc.). Le vaste panel de possibilités nous permet de trouver des outils adaptés avec toujours plus d'originalité.

Cependant, ce choix peut-être stressant : lorsque l'on choisit un framework, il faut suivre la méthodologie et les recommandations qui l'accompagnent, qui sont parfois incohérentes avec notre projet. Les recherches ne sont plus "comment faire ceci" mais "comment empêcher **X** de faire cela". Ce phénomène est appelé la **Framework Fatigue**, il existe pour tous les langages mais est particulièrement important en Javascript, d'ailleurs souvent nommé **Javascript Fatigue** pour ce dernier. Je recommande l'article en anglais ["Design: #noFramework"](https://javarome.medium.com/design-noframework-bbc00a02d9b3) qui explique très bien ce phénomène.

Pour JS, ce mouvement a commencé avec [Vanilla JS](https://vanilla-js.com), qui pousse à utiliser autant que possible les solutions de base offertes par nos outils, n'utilisant que des librairies reconnues pour des cas spécifiques afin d'éviter de réinventer la roue. Le mouvement est en pleine croissance ces dernières années avec la montée exponentielle de l'écosystème.

Le fait de se limiter aux fonctionnalités natives ralentit souvent le développement, mais offre des avantages incontestables et puissants. La non-utilisation de framework permet de structurer un projet sur-mesure à sa problématique; utiliser les APIs natives offre beaucoup plus de contrôle et de possibilités, et permet de mieux comprendre les techniques derrière le fonctionnement des librairies que l'on utilise; les performances sont améliorées car on évite d'importer de lourdes dépendances consommatrices de ressources. Un bel exemple est donné dans l'article suivant, ["What Web Frameworks Solve: The Vanilla Alternative (Part 2)"](https://www.smashingmagazine.com/2022/02/web-frameworks-guide-part2/).

<hr />

## Mon expérience

Je travaille avec JavaScript depuis le début de mes études dans le domaine de l'informatique. J'ai donc utilisé ce langage dans de nombreux projets. Le premier dont je veux faire part est **Valup**, une librairie de programmation réactive que j'ai créé afin de mieux comprendre la réactivité. C'est un projet relativement simple pour les connaisseurs mais que je trouve tout de même intéressant à étudier. Je ne vais pas m'étendre sur ce projet ici car j'ai déjà rédigé un article en anglais dédié à cette librairie : ["Writing a reactive library in JavaScript [from scratch]"](https://dev.to/adea/writing-a-reactive-library-in-javascript-from-scratch-1k1i)

Un autre projet dont je suis fier est **Chatbot-IO**. Cette application en JS vanilla pur permet de communiquer avec des bots depuis une interface web ressemblant à Discord. Là où de nombreux développeurs auraient opté pour une programmation orienté objet et des frameworks tels que React ou Vue, j'ai décidé de faire au plus simple. Un simple serveur http en NodeJS qui envoie des _Server-Sent Events_ dès qu'un message est reçu. En utilisant cette technique j'ai pu réduire drastiquement l'utilisation de ressources pour cette application, et montrer que JavaScript peut être utilisé pour des projets sérieux sans avoir de déficit de performance.

Ce langage est crucial, car son environnement me permet de développer sans problèmes de nombreux outils sans difficulté dans l'entreprise. Sa syntaxe et sa facilité me permettent d'échanger avec les administrateurs systèmes et administrateurs de bases de données qui sont généralement capables de lire et comprendre en partie mon code, facilitant ainsi la collaboration lorsque nos compétences respectives doivent être liées.

<hr />

## Compétence

J'ai une maîtrise du langage : je ne suis en aucun cas un expert, mais je suis assez autonome et instruit sur le langage et son fonctionnement pour développer des solutions complexes. Je connais les ressources à utiliser, les APIs natives, je suis activement l'actualité concernant ce domaine et j'essaye d'y contribuer.

Il me reste encore beaucoup à apprendre. Je n'ai pas encore utilisé beaucoup d'APIs natives comme WebGL pour la modélisation 3D ou WebVR pour la réalité virtuelle, principalement par manque de temps et de cas d'utilisation. Je n'ai pas encore développé d'applications mobiles avec, ni même de **Progressive Web Apps** que je comprends mais qui ne me servent pas à l'heure actuelle. Enfin, j'ai peu d'expérience des frameworks, même des plus connus que je délaisse souvent au profit d'une solution vanilla.

Dans mon métier, il faut pouvoir s'adapter à toute situation et avoir des compétences dans chaque domaine du cycle de vie d'un logiciel. En cela, JavaScript est une force inestimable. Premièrement car c'est le langage majeur dans le développement web côté client (il existe bien [Elm](https://elm-lang.org), [Rescript](https://rescript-lang.org) ou [Purescript](https://www.purescript.org), mais ces langages sont ensuite compilés en JavaScript). Deuxièmement car le web est multiplateforme et que de nombreux outils permettent d'adapter un projet web en application native. Troisièmement parce que c'est un langage de scripting, il permet donc de créer des scripts importants dans la gestion des pipelines en bénéficiant de tout son écosystème.

Au début de ma formation, en septembre 2018, c'est le premier langage de programmation que j'ai appris.
Au début, c'était difficile, la contrainte que nous avions de ne pas utiliser de librairie ou de framework (excepté [Node.js](https://nodejs.org)) rendait la tâche bien compliquée. À l'époque, les normes ES5 et ES6 du langage n'étaient pas encore universellement adoptées, des APIs comme **fetch** n'étaient pas encore accessible, [jQuery](https://jquery.com) était encore un outil important par la simplicité qu'il apportait. Mais en persévérant, j'ai réussi à développer mes premières applications client-serveur. Je continue d'apprendre autant et aussi rapidement que possible. Mes collègues relèvent régulièrement mes capacités d'auto-formation qui les impressionnent.

Après quelques années d'utilisation de JavaScript, l'écosystème a de nombreux changements, dont l'adoption massive des standards développés depuis 2015 ; et la collaboration entre les navigateurs ayant le plus de parts de marché afin d'essayer d'offrir une meilleure expérience de développement en unifiant les implémentations du langage. Autre chose à noter : l'accélération de la croissance de l'écosystème de librairies et d'outils, les technologies sont de plus en plus volatiles.

Si j'avais à refaire ce chemin, j'aurais mieux choisi mes ressources, et j'aurais directement commencé avec des librairies de facilité (jQuery, Lodash, etc.) mais toujours sans framework. Utiliser la base de cette technologie a été un plus dans ma formation. Si j'avais été aidé à mes débuts avec des librairies telles que jQuery pour la manipulation de l'interface, Lodash pour les opérations courantes sur des structures de données ou encore [Express](https://expressjs.com) pour rapidement développer une API, j'aurais certainement pu aller encore bien plus loin et bien plus rapidement. Aujourd'hui je ne conseillerai plus jQuery car le sélecteur est devenu facultatif avec `document.querySelectorAll()` et que les requêtes asynchrones sont facilitées par l'API **fetch**. Je conseille encore Lodash et Express pour débuter, et j'aimerais pousser ceux qui les utilisent à les laisser de côté. Ces librairies sont très efficaces et plaisantes à utiliser, mais il est impératif, à partir d'un certain niveau, d'être capable de s'en débarrasser au besoin ou à la demande, donc elles ne sont utiles que si l'on comprend réellement le fonctionnement interne de celles-ci.

Comme précisé sur ma page de présentation, je m'inscris dans le mouvement DevOps. Gérer le cycle de vie d'un logiciel de A à Z est rarement une partie de plaisir, mais très gratifiant lorsque l'on réussi. Je veux être capable de tout réaliser, même à un niveau intermédiaire. Pour cela, j'aimerais encore pousser mon niveau en JavaScript : apprendre des fonctionnalités plus obscures, faire de l'algorithmie intense nécessitant une optimisation des ressources, et programmer en bas niveau en développant mon propre langage de programmation basé sur JavaScript.

Afin de m'améliorer sur mon langage préféré, je vais suivre prochainement des formations pour continuer d'apprendre, j'ai plusieurs objectifs. Premièrement, apprendre la modélisation avec la librairie [Three.js](https://threejs.org) puis directement en WebGL. Deuxièmement, me former à d'autres systèmes de communications utilisant différents protocoles comme [gRCP](https://grcp.io) ou [MessagePack](https://msgpack.org) depuis JavaScript. Enfin, développer des jeux vidéos ou autres démos interactives en JavaScript. Ces trois domaines pointus me permettront d'augmenter grandement ma maîtrise du langage.
